package io.hotmail.com.jacob_vejvoda.ElderGuardianBoss;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.block.Banner;
import org.bukkit.block.banner.Pattern;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

@SuppressWarnings("deprecation")
public class ElderGuardianBoss_old extends JavaPlugin implements Listener{
		HashMap<Entity, BossBar> bossMap = new HashMap<Entity, BossBar>();
		//HashMap<String, BossBar> playerBars = new HashMap<String, BossBar>();
		
	    @Override
	    public void onEnable(){
	    	getServer().getPluginManager().registerEvents(this, this); 
	    	if (!new File(getDataFolder(), "config.yml").exists()) {
	   	     saveDefaultConfig();
	    	}
			try {
			    Metrics metrics = new Metrics(this);
			    metrics.start();
			} catch (IOException e) {
			    // Failed to submit the stats :-(
			}
	    	timer();
	    }
	    
	    @SuppressWarnings({ "unchecked" })
		public void timer(){
	    	try{
	    		HashMap<Entity, BossBar> tmp = (HashMap<Entity, BossBar>) bossMap.clone();
	    		for (Map.Entry<Entity, BossBar> hm : tmp.entrySet()){
	    			Entity e = hm.getKey();
		    		//Dead Check
		    		if(((Damageable)e).getHealth() <= 0)
		    			bossMap.remove(e);
		    		//Special Attack
		    		int min = 1;
		    		int max = getConfig().getInt("specialChance");
					int r = new Random().nextInt(max - min + 1) + min;
					if(r == 1){
						int count = 0;
						boolean nearby = false;
						for(Entity ent : e.getNearbyEntities(25, 25, 25))
							if(ent instanceof Guardian){
								count = count + 1;
							}else if(ent instanceof Player)
								nearby = true;
						//System.out.println("Count = " + count);
						if(nearby && (count < getConfig().getInt("maxSpecial")))
							e.getWorld().spawnEntity(e.getLocation(), EntityType.GUARDIAN);
					}
		    	}
		    	for(Player p : Bukkit.getServer().getOnlinePlayers()){
		    		//Bar Add
		    		double dis = 25 + 1;
		    		Entity b = null;
		    		for (Map.Entry<Entity, BossBar> hm : bossMap.entrySet()){
		    			Entity boss = hm.getKey();
		    			if(p.getWorld().equals(boss.getWorld()))
			    			if(p.getLocation().distance(boss.getLocation()) < dis){
			    				dis = p.getLocation().distance(boss.getLocation());
			    				b = boss;
			    			}
		    		}
		    		//System.out.println("B: " + b);
		    		if(b != null){
	    				showBossBar(p, b);
		    		}else
		    			removeBar(p);
		    	}
	    	}catch(Exception x){x.printStackTrace();}
	    	//Tick
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
				public void run() {
					timer();
				}
			}, (20*1));
	    }
	    
	    private void removeBar(Player p){
	    	for (Map.Entry<Entity, BossBar> hm : bossMap.entrySet())
	    		if(hm.getValue().getPlayers().contains(p)){
	    			hm.getValue().removePlayer(p);
	    		}
//	    	if(playerBars.containsKey(p.getName())){
//	    		playerBars.get(p.getName()).removePlayer(p);
//	    		playerBars.remove(p.getName());
//	    	}
	    }
	    
		public void showBossBar(Player p, Entity e){
            BossBar bar = bossMap.get(e);
            if(!bar.getPlayers().contains(p))
            	bar.addPlayer(p);
            updateHP(e);
	    }
		
		private void updateHP(Entity e){
            BossBar bar = bossMap.get(e);
        	float health = (float) ((Damageable) e).getHealth();
        	float maxHealth = (float) ((Damageable) e).getMaxHealth();
        	float setHealth = (health * 100.0f) / maxHealth;
        	bar.setProgress(setHealth/100.0f);
		}
	    
		public void makeBoss(Entity ent){
			String title = ChatColor.translateAlternateColorCodes('&', getConfig().getString("bossName"));
        	BossBar bar = Bukkit.createBossBar(title, BarColor.valueOf(getConfig().getString("barColor")), BarStyle.valueOf(getConfig().getString("barStyle")), BarFlag.CREATE_FOG);
        	bar.setVisible(true);
	    	bossMap.put(ent, bar);
	    	int maxHP = getConfig().getInt("bossHealth");
	    	if(((Damageable)ent).getMaxHealth() != maxHP){
	    		((Damageable)ent).setMaxHealth(maxHP);
	    		((Damageable)ent).setHealth(maxHP);
	    	}
	    }
	    
		@EventHandler(priority=EventPriority.HIGH)
		public void onChunkLoad(ChunkLoadEvent e) {
			for(Entity ent : e.getChunk().getEntities())
				if(ent instanceof Guardian)
					if(((Guardian)ent).isElder())
						if(bossMap.containsKey(ent) == false)
							makeBoss(ent);
		}
		
		@EventHandler(priority=EventPriority.HIGH)
		public void onEntitySpawn(EntitySpawnEvent e) {
			Entity ent = e.getEntity();
			if(ent instanceof Guardian)
				if(((Guardian)ent).isElder())
					if(bossMap.containsKey(ent) == false)
						makeBoss(ent);
		}
		
		@SuppressWarnings("unchecked")
		@EventHandler(priority=EventPriority.HIGH)
		public void onEntityDamaged(EntityDamageByEntityEvent e) {
			Entity ent = e.getEntity();
			if(ent instanceof Guardian){
				if(((Guardian)ent).isElder())
					if(bossMap.containsKey(ent) == false){
						makeBoss(ent);
					}else
						updateHP(ent);
			}else if(e.getDamager() instanceof Guardian)
				if(((Guardian)e.getDamager()).isElder()){
					//Guardian Boss Damage
					for(String pn : (ArrayList<String>)getConfig().getList("bossLaserPotions")){
						String[] split = pn.split(":");
						if(PotionEffectType.getByName(split[0]) != null)
							((LivingEntity)ent).addPotionEffect(new PotionEffect(PotionEffectType.getByName(split[0]), Integer.parseInt(split[1]),  Integer.parseInt(split[2])-1), true);
					}
				}
		}
		
		@EventHandler(priority=EventPriority.HIGH)
		public void onEntityDeath(EntityDeathEvent e) {
			Entity ent = e.getEntity();
			if(ent instanceof Guardian)
				if(((Guardian)ent).isElder())
					if(bossMap.containsKey(ent)){
						//Boss Death
						for(Player p : bossMap.get(ent).getPlayers())
							bossMap.get(ent).removePlayer(p);
						bossMap.remove(ent);
						//System.out.println("DXP: " + e.getDroppedExp());
						e.setDroppedExp(getConfig().getInt("dropedXP"));
						if(getConfig().getBoolean("dropLoot"))
							ent.getWorld().dropItemNaturally(ent.getLocation(), getLoot());
						//ParticleEffects_1_9.sendToLocation(ParticleEffects_1_9.CLOUD, ent.getLocation(), 0, 0, 0, 1, 50);
						displayParticle("CLOUD", ent.getLocation(), 0, 1, 50);
					}
		}
		
		public void displayParticle(String effect, Location l, double radius, int speed, int amount){
			displayParticle(effect, l.getWorld(), l.getX(), l.getY(), l.getZ(), radius, speed, amount);
		}
		
	    private void displayParticle(String effect, World w, double x, double y, double z, double radius, int speed, int amount) {
	        amount = (amount <= 0) ? 1 : amount;
	        Location l = new Location(w, x, y, z);
	        try {
	            if (radius <= 0) {
	                w.spawnParticle(Particle.valueOf(effect), l, 0, 0, 0, speed, amount);
	            } else {
	                List<Location> ll = getArea(l, radius, 0.2);
	                if (ll.size() > 0){
	                    for (int i = 0; i < amount; i++) {
	                        int index = new Random().nextInt(ll.size());
	                        w.spawnParticle(Particle.valueOf(effect), ll.get(index), 1, 0, 0, speed, 1);
	                        ll.remove(index);
	                    }
	                }
	            }
	        } catch (Exception ex) {
	            //System.out.println("V: " + getServer().getVersion());
	            ex.printStackTrace();
	        }
	    }
		  
		private ArrayList<Location> getArea(Location l, double r, double t){
			ArrayList<Location> ll = new ArrayList<Location>();
				   for (double x = l.getX() - r; x < l.getX() + r; x += t) {
					   for (double y = l.getY() - r; y < l.getY() + r; y += t) {
						   for (double z = l.getZ() - r; z < l.getZ() + r; z += t) {
							   ll.add(new Location(l.getWorld(), x, y, z));
						   }
					   }
				   }
				   return ll;
		}
		
		public ItemStack getLoot(){
//			ItemStack s = new ItemStack(Material.DIAMOND_SWORD);
//			ItemMeta m = s.getItemMeta();
//			m.setDisplayName("§bPrismarine Sword");
//			m.setLore(new ArrayList<String>(Arrays.asList("§3Dropped from an Elder Guardian")));
//			s.setItemMeta(m);
//			//Rand Enchant
//			int min = 1;
//			int max = 4;
//			int r = new Random().nextInt(max - min + 1) + min;
//			ArrayList<Enchantment> ench = new ArrayList<Enchantment>(Arrays.asList(Enchantment.DAMAGE_ALL, Enchantment.DAMAGE_ARTHROPODS, Enchantment.DAMAGE_UNDEAD, Enchantment.DURABILITY, Enchantment.FIRE_ASPECT, Enchantment.KNOCKBACK, Enchantment.LOOT_BONUS_MOBS));
//			for(int i = 0; i <= r; i++){
//				int max2 = 10;
//				int level = new Random().nextInt(max2 - min + 1) + min;
//				int index = new Random().nextInt((ench.size()-1) - 0 + 1) + 0;
//				s.addUnsafeEnchantment(ench.get(index), level);
//			}
			List<String> list = new ArrayList<String>(getConfig().getConfigurationSection("loot").getKeys(false));
			//System.out.println("List: " + list);
			String loot = list.get(new Random().nextInt(list.size()));
			ItemStack s = getItem(loot);
			return s;
		}
	    
	    @SuppressWarnings({ "unchecked" })
		public ItemStack getItem(String loot) {
	        //System.out.println("Get Loot: " + loot);
	        try {
	            String setItem = this.getConfig().getString("loot." + loot + ".item");

	            String setAmountString = this.getConfig().getString("loot." + loot + ".amount");
	            int setAmount;
	            if (setAmountString != null) {
	                setAmount = getIntFromString(setAmountString);
	            } else
	                setAmount = 1;
	            ItemStack stack = new ItemStack(Material.valueOf(setItem), setAmount);
	            if (this.getConfig().getString("loot." + loot + ".durability") != null) {
	                String durabilityString = this.getConfig().getString("loot." + loot + ".durability");
	                int durability = getIntFromString(durabilityString);
	                stack.setDurability((short) durability);
	            }
				//Get Durability
				if(getConfig().getString("loot." +loot + ".durability") != null)
					stack.setDurability((short)getConfig().getInt("loot." + ".durability"));
				//Get Name
				String name = null;
				//System.out.println("1");
				if(getConfig().getList("loot." +loot + ".name") != null){
					//System.out.println("2");
					ArrayList<String> names = (ArrayList<String>) getConfig().getList("loot." +loot + ".name");
					if(names != null){
						name = names.get(rand(1, names.size())-1);
						name = prosessLootName(name, stack);
					}
				}else if(getConfig().getString("loot." +loot + ".name") != null){
					//System.out.println("3");
					name = getConfig().getString("loot." +loot + ".name") ;
			        name = prosessLootName(name, stack);
				}
				//System.out.println("4");
				//Get Lore
				ArrayList<String> loreList = new ArrayList<String>();
				for(int i = 0; i <= 10; i++) {
					if (getConfig().getString("loot." +loot + ".lore" + i) != null){
						String lore = (getConfig().getString("loot." +loot + ".lore" + i));
						lore = ChatColor.translateAlternateColorCodes('&', lore);	
						loreList.add(lore);
						System.out.println("5");
					}
				}
				//System.out.println("6");
				if (getConfig().getList("loot."+loot + ".lore") != null){
					//System.out.println("7");
					ArrayList<String> lb = (ArrayList<String>)getConfig().getList("loot."+loot + ".lore");
					ArrayList<String> l = (ArrayList<String>) lb.clone();
					int min = l.size();
					if (getConfig().getString("loot."+ loot +".minLore") != null)
						min = getConfig().getInt("loot."+ loot +".minLore");
					int max = l.size();
					if (getConfig().getString("loot."+loot + ".maxLore") != null)
						max = getConfig().getInt("loot."+loot + ".maxLore");
					if(!l.isEmpty())
						for(int i = 0; i < rand(min,max); i++){
							String lore = l.get(rand(1,l.size())-1);
							l.remove(lore);
							loreList.add(prosessLootName(lore, stack));
						}
				}
				//System.out.println("8");
	            ItemMeta meta = stack.getItemMeta();
	            if (name != null) {
	                meta.setDisplayName(name);
					//System.out.println("9");
	            }
	            if (!loreList.isEmpty()) {
	                meta.setLore(loreList);
					//System.out.println("10");
	            }
	            stack.setItemMeta(meta);
	            //Colour
	            if (this.getConfig().getString("loot." + loot + ".colour") != null && stack.getType().toString().toLowerCase().contains("leather")) {
	                String c = this.getConfig().getString("loot." + loot + ".colour");
	                String[] split = c.split(",");
	                Color colour = Color.fromRGB(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
	                dye(stack, colour);
	            }
	            //Book
	            if ((stack.getType().equals(Material.WRITTEN_BOOK)) || (stack.getType().equals(Material.WRITABLE_BOOK))) {
	                BookMeta bMeta = (BookMeta) stack.getItemMeta();
	                if (this.getConfig().getString("loot." + loot + ".author") != null) {
	                    String author = this.getConfig().getString("loot." + loot + ".author");
	                    author = ChatColor.translateAlternateColorCodes('&', author);
	                    bMeta.setAuthor(author);
	                }
	                if (this.getConfig().getString("loot." + loot + ".title") != null) {
	                    String title = this.getConfig().getString("loot." + loot + ".title");
	                    title = ChatColor.translateAlternateColorCodes('&', title);
	                    bMeta.setTitle(title);
	                }
	                if (this.getConfig().getString("loot." + loot + ".pages") != null) {
	                    for (String i : this.getConfig().getConfigurationSection("loot." + loot + ".pages").getKeys(false)) {
	                        String page = this.getConfig().getString("loot." + loot + ".pages." + i);
	                        page = ChatColor.translateAlternateColorCodes('&', page);
	                        bMeta.addPage(page);
	                    }
	                }
	                stack.setItemMeta(bMeta);
	            }
	            //Banners
	            if (stack.getType().toString().contains("BANNER")) {
	                BannerMeta b = (BannerMeta) stack.getItemMeta();
	                List<Pattern> patList = (List<Pattern>) getConfig().getList("loot." + loot + ".patterns");
	                if (patList != null && (!patList.isEmpty()))
	                    b.setPatterns(patList);
	                stack.setItemMeta(b);
	            }
	            //Shield
	            if (stack.getType().equals(Material.SHIELD)) {
	                ItemMeta im = stack.getItemMeta();
	                BlockStateMeta bmeta = (BlockStateMeta) im;

	                Banner b = (Banner) bmeta.getBlockState();
	                List<Pattern> patList = (List<Pattern>) getConfig().getList("loot." + loot + ".patterns");
	                b.setBaseColor(DyeColor.valueOf(getConfig().getString("loot." + loot + ".colour")));
	                b.setPatterns(patList);
	                b.update();
	                bmeta.setBlockState(b);
	                stack.setItemMeta(bmeta);
	            }
	            //Owner
	            if ((stack.getType().equals(Material.PLAYER_HEAD)) && (stack.getDurability() == 3)) {
	                String owner = this.getConfig().getString("loot." + loot + ".owner");
	                SkullMeta sm = (SkullMeta) stack.getItemMeta();
	                sm.setOwner(owner);
	                stack.setItemMeta(sm);
	            }
	            //Potions
	            if (getConfig().getString("loot." + loot + ".potion") != null)
	                if (stack.getType().equals(Material.POTION) || stack.getType().equals(Material.SPLASH_POTION) || stack.getType().equals(Material.LINGERING_POTION)) {
	                    PotionMeta pMeta = (PotionMeta) stack.getItemMeta();
	                    String pn = getConfig().getString("loot." + loot + ".potion");
	                    pMeta.setBasePotionData(new PotionData(PotionType.getByEffect(PotionEffectType.getByName(pn)), false, false));
	                    stack.setItemMeta(pMeta);
	                }
	            int enchAmount = 0;
	            for (int e = 0; e <= 10; e++) {
	                if (this.getConfig().getString("loot." + loot + ".enchantments." + e) != null) {
	                    enchAmount++;
	                }
	            }
	            if (enchAmount > 0) {
	                int enMin = enchAmount;
	                int enMax = enchAmount;
	                if ((this.getConfig().getString("loot." + loot + ".minEnchantments") != null) && (this.getConfig().getString("loot." + loot + ".maxEnchantments") != null)) {
	                    enMin = this.getConfig().getInt("loot." + loot + ".minEnchantments");
	                    enMax = this.getConfig().getInt("loot." + loot + ".maxEnchantments");
	                }
	                int enchNeeded = new Random().nextInt(enMax + 1 - enMin) + enMin;
	                if (enchNeeded > enMax) {
	                    enchNeeded = enMax;
	                }
	                ArrayList<LevelledEnchantment> enchList = new ArrayList<LevelledEnchantment>();
	                int safety = 0;
	                int j = 0;
	                int chance;
	                do {
	                    if (this.getConfig().getString("loot." + loot + ".enchantments." + j) != null) {
	                        int enChance = 1;
	                        if (this.getConfig().getString("loot." + loot + ".enchantments." + j + ".chance") != null) {
	                            enChance = this.getConfig().getInt("loot." + loot + ".enchantments." + j + ".chance");
	                        }
	                        chance = new Random().nextInt(enChance - 1 + 1) + 1;
	                        if (chance == 1) {
	                            String enchantment = this.getConfig().getString("loot." + loot + ".enchantments." + j + ".enchantment");

	                            String levelString = this.getConfig().getString("loot." + loot + ".enchantments." + j + ".level");
	                            int level = getIntFromString(levelString);
	                            if (Enchantment.getByName(enchantment) != null) {
	                                if (level < 1) {
	                                    level = 1;
	                                }
	                                LevelledEnchantment le = new LevelledEnchantment(Enchantment.getByName(enchantment), level);

	                                boolean con = false;
	                                for (LevelledEnchantment testE : enchList) {
	                                    if (testE.getEnchantment.equals(le.getEnchantment)) {
	                                        con = true;
	                                    }
	                                }
	                                if (!con) {
	                                    enchList.add(le);
	                                }
	                            } else {
	                                System.out.println("Error: No valid drops found!");
	                                System.out.println("Error: " + enchantment + " is not a valid enchantment!");
	                                return null;
	                            }
	                        }
	                    }
	                    j++;
	                    if (j > enchAmount) {
	                        j = 0;
	                        safety++;
	                    }
	                    if (safety >= enchAmount * 100) {
	                        System.out.println("Error: No valid drops found!");
	                        System.out.println("Error: Please increase chance for enchantments on item " + loot);
	                        return null;
	                    }
	                } while (enchList.size() != enchNeeded);
	                for (LevelledEnchantment le : enchList) {
	                    if (stack.getType().equals(Material.ENCHANTED_BOOK)) {
	                        EnchantmentStorageMeta enchantMeta = (EnchantmentStorageMeta) stack.getItemMeta();
	                        enchantMeta.addStoredEnchant(le.getEnchantment, le.getLevel, true);
	                        stack.setItemMeta(enchantMeta);
	                    } else {
	                        stack.addUnsafeEnchantment(le.getEnchantment, le.getLevel);
	                    }
	                }
	            }
	            return stack;
	        } catch (Exception e) {
	            this.getLogger().log(Level.SEVERE, e.getMessage(), true);
	            e.printStackTrace();
	        }
	        return null;
	    }
		
		public int rand(int min, int max){
			int r = min + (int)(Math.random() * (1 + max - min));
			return r;
		}
		
		  private String prosessLootName(String name, ItemStack stack){
		      name = ChatColor.translateAlternateColorCodes('&', name);
		      String itemName = stack.getType().name();
		      itemName = itemName.replace("_", " ");
		      itemName = itemName.toLowerCase();
		      name = name.replace("<itemName>", itemName);
		      return name;
		  }
		
	    public void dye(ItemStack item, Color color){
	    	try{
				LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
				meta.setColor(color);
				item.setItemMeta(meta);
	    	}catch(Exception e){}
	    }
		
		public int getIntFromString(String setAmountString){
			int setAmount = 1;
			if(setAmountString.contains("-")){
			    String[] split = setAmountString.split("-");
			    try{
			    	Integer minSetAmount = Integer.parseInt(split[0]);
			    	Integer maxSetAmount = Integer.parseInt(split[1]);
			    	setAmount = new Random().nextInt(maxSetAmount - minSetAmount + 1) + minSetAmount;
			    }catch(Exception e){System.out.println("getIntFromString: " + e);}
			}else{
				setAmount = Integer.parseInt(setAmountString);
			}
			return setAmount;
		}
		
		public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
	    	if((cmd.getName().equals("elderguardianboss")) || (cmd.getName().equals("egb"))){
	    		try{
		    		if(args[0].equals("reload")){
		    			reloadConfig();
		    			sender.sendMessage("§eEGB: Reloaded config!");
		    			return true; 
		    		}else if(args[0].equals("spawn")){
		    			if(sender instanceof Player){
		    				Player p = (Player) sender;
		    				Location l = p.getTargetBlock((Set<Material>)null, 100).getLocation();
		    				l.setY(l.getY()+1);
		    				Guardian g = (Guardian)l.getWorld().spawnEntity(l, EntityType.GUARDIAN);
		    				g.setElder(true);
		    				sender.sendMessage("§eEGB: Spawned a Elder Guardian boss!");
		    			}
		    			return true; 
		    		}else if(args[0].equals("loot")){
		    			if(sender instanceof Player){
		    				Player p = (Player) sender;
			    			ExperienceOrb orb = (ExperienceOrb) p.getWorld().spawnEntity(p.getLocation(), EntityType.EXPERIENCE_ORB);
							orb.setExperience(getConfig().getInt("dropedXP"));
							if(getConfig().getBoolean("dropLoot"))
								p.getWorld().dropItemNaturally(p.getLocation(), getLoot());
							sender.sendMessage("§eEGB: Dropped Elder Guardian boss loot!");
		    			}
		    			return true; 
		    		}
	    		}catch(Exception e){}
    			sender.sendMessage("§6--- Elder Guardian Boss v" + Bukkit.getServer().getPluginManager().getPlugin("ElderGuardianBoss").getDescription().getVersion() + " ---");
    			sender.sendMessage("§e/egb spawn   <- Spawns a Elder Guardian Boss");
    			sender.sendMessage("§e/egb loot      <- Drops the Elder Guardian's death loot");
    			sender.sendMessage("§e/egb reload  <- Re-loads the config");
	    	}
	    	return true; 
	    }
	    
	    class LevelledEnchantment {
	    	public Enchantment getEnchantment;
	    	public int getLevel;
	    	LevelledEnchantment (Enchantment enchantment, int level) {
	    		getEnchantment = enchantment;
	    		getLevel = level;

	    	}
	    }
}